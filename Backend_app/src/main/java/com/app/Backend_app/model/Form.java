package com.app.Backend_app.model;

import javax.persistence.*;

@Entity

@Table(name = "FormTable")
public class Form {

    @Id
    @Column(name = "_Id")
    private int id;

    @Column(name = "_Marque")
    private String marque;

    @Column(name = "_Annee")
    private int annee;

    @Column(name = "_Vitesse")
    private int vitesse;

    @Column(name = "_Puissance")
    private int puissance;

    @Column(name = "_Couleur")
    private String couleur;
    @Column(name = "_Image")
    private String image;
    @Column(name = "_Prix")
    private long prix;


    public Form(int id, String marque, int annee, int vitesse, int puissance, String couleur, long prix, String image) {
        this.id = id;
        this.marque = marque;
        this.annee = annee;
        this.vitesse = vitesse;
        this.puissance = puissance;
        this.couleur = couleur;
        this.prix =prix;
        this.image =image;

    }

    public Form(){

    }
    public long getPrix() {
        return prix;
    }

    public void setPrix(long prix) {
        this.prix = prix;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public int getVitesse() {
        return vitesse;
    }

    public void setVitesse(int vitesse) {
        this.vitesse = vitesse;
    }

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }


}

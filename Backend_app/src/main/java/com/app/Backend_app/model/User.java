package com.app.Backend_app.model;

import lombok.Builder;

import javax.persistence.*;

@Entity
@Builder
@Table(name = "TableUser")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "_Id")
    private int id;
    @Column(name = "_emailId")
    private String emailId;
    @Column(name = "_userName")
    private String userName;
    @Column(name = "_password")
    private String password;
    public User(int id, String emailId, String userName, String password) {
        this.id = id;
        this.emailId = emailId;
        this.userName = userName;
        this.password = password;
    }
    public User(){
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getEmailId() {
        return emailId;
    }
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



}

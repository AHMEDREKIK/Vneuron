package com.app.Backend_app.repository;

import com.app.Backend_app.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegistrationRepository extends JpaRepository<User,Integer> {
    public User findByEmailId(String emailId);
    public User findByEmailIdAndPassword(String emailId,String password);

}

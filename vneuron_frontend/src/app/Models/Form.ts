export class Form {
  id!:number;
  marque!:string;
  annee!:number;
  vitesse!:number;
  puissance!:number;
  couleur!:string;
  prix!:number;
  image!:string;
}

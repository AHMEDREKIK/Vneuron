import { FormService } from './../form.service';
import { Component } from '@angular/core';
import { OnInit} from '@angular/core'
import {MatDialog} from '@angular/material/dialog';
import { DialogFormComponent } from '../dialog-form/dialog-form.component';
import { Form } from '../Models/Form';

@Component({
  selector: 'app-cart-form',
  templateUrl: './cart-form.component.html',
  styleUrls: ['./cart-form.component.css']
})
export class CartFormComponent implements OnInit {

  forms : any = [];
  num_id!: number;
  references: any = [];

  constructor(private _formService:FormService,private _dialog:MatDialog){
  }

  ngOnInit(): void {
    this._formService.getAllForms()
    .subscribe(res=>{
      this.forms = res;
    })

    this._formService.getAllReferences().subscribe(
      res =>{console.log("reference recieved");
      this.references = res;
    })
  }

  openDialog(form:Form) {
    this.num_id = form.id;
    this._dialog.open(DialogFormComponent,{data: form});
  }

}

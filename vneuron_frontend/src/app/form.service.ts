import { Form } from './Models/Form';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { ReferenceForm } from './Models/ReferenceForm';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(private _http:HttpClient) { }

  public getAllForms():Observable<any>{
    return  this._http.get<any>("http://localhost:8090/allforms").pipe(map((res:any)=>{
      return res;
    }))
  }

  public sendModifiedForm(form: Form):Observable<Form>{
    return this._http.post<Form>("http://localhost:8090/modifiedform",form)
  }

  public sendReferenceForm(modelreference:any):Observable<ReferenceForm>{
    return this._http.post<ReferenceForm>("http://localhost:8090/modifiedreference",modelreference)
  }

  public getAllReferences():Observable<any>{
    return  this._http.get<any>("http://localhost:8090/allreferences").pipe(map((res:any)=>{
      return res;
    }))
  }

}

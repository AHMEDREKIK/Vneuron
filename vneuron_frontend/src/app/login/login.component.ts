import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { RegistrationService } from './../registration.service';
import { Component } from '@angular/core';
import { User } from '../user';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  user =new User();
  msg ='';
  constructor(private _service:RegistrationService ,private _router:Router, private toastr: ToastrService, private auth: AuthService){}
  loginUser(){
    this._service.loginUserFromRemote(this.user).subscribe(
      data =>{console.log("response recieved");
      this.auth.IsLoggedIn = true;
      this._router.navigate(['/loginsuccess']);
      this.toastr.success("Congrations!", "You have logged in successfully.", {
        titleClass: "center",
        messageClass: "center"
      });
        },error=>{ console.log("exception occured");
        this.toastr.error("Something is wrong, please verify.", 'Major Error');
    this.msg ="Bad credentials, please enter valid email and password";})
 }
 gotoregistration(){
  this._router.navigate(['/registration'])
 }

}

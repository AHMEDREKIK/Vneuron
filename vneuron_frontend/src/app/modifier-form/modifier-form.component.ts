import { Router } from '@angular/router';
import { FormService } from './../form.service';
import { DialogFormComponent } from './../dialog-form/dialog-form.component';
import { Component, Inject, OnInit } from '@angular/core';
import { Form } from '../Models/Form';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({

  selector: 'app-modifier-form',
  templateUrl: './modifier-form.component.html',
  styleUrls: ['./modifier-form.component.css']
})
export class ModifierFormComponent implements OnInit{
  voitureForm!:FormGroup ;
  modelForm!:FormGroup;
  modelReference!:FormGroup;
  nb!:number;



  ngOnInit() : void{
    this.voitureForm =this.fb.group({
      annee:[this.form.annee,Validators.required],
      vitesse:[this.form.vitesse,Validators.required],
      puissance:[this.form.puissance,Validators.required],
      couleur:[this.form.couleur,Validators.required],
      prix:[this.form.prix,Validators.required],

    });
    this.modelReference =this.fb.group({
      marque:[this.form.marque],
      references:this.fb.array([])
    });


  }



  constructor(@Inject(MAT_DIALOG_DATA) public form: Form, private _dialog:MatDialog,private fb: FormBuilder, private auth: AuthService ,private _formService: FormService, private route: Router) {
    this.route.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  closeModifyDialogOpenDialog(){
    this._dialog.closeAll();
    this._dialog.open(DialogFormComponent,{data: this.form})
  }
  reference(): any {
    return this.fb.group({
      reference: this.fb.control(''),
    });
  }


  submitNewForm(){


    this.modelForm =this.fb.group({
      id:[this.form.id],
      image:[this.form.image],
      marque:[this.form.marque],
      annee:[this.voitureForm.controls['annee'].value],
      vitesse:[this.voitureForm.controls['vitesse'].value],
      puissance:[this.voitureForm.controls['puissance'].value],
      couleur:[this.voitureForm.controls['couleur'].value],
      prix:[this.voitureForm.controls['prix'].value]
    })

     this._formService.sendReferenceForm(this.modelReference.value).subscribe(
      data=> { console.log("Reference is modified");
    })


    this._formService.sendModifiedForm(this.modelForm.value).subscribe(
      data => { console.log("Form is modified");
      this._dialog.closeAll();
      this.route.navigate(["/loginsuccess"]);
    })
  }
   get referenceFieldAsFormArray():FormArray{
    return this.modelReference.controls['references'] as FormArray;
  }


  addControl():void{
    this.referenceFieldAsFormArray.push(this.reference())

  }
  deletereference(index:number):void{
    this.referenceFieldAsFormArray.removeAt(index);
    this.referenceFieldAsFormArray.markAsDirty();

  }

}
